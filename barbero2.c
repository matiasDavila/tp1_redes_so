#include<stdio.h>
#include<pthread.h>
#include<semaphore.h>
# include <unistd.h>
# include <string.h>

int cantidad_clientes=0;
int estado_barber=0;
int sillas=3;

sem_t sembarbero;
sem_t semcliente;
sem_t clientes_mutex;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

static void * thread_barber_function(void* arg){
  while(1){
    
    if(cantidad_clientes==0){
      estado_barber=0;
	    printf("ZZZZZZZZZZZZzzzzzzzzzzzzzz \n");
      printf("estado del barbero barbero.0 %d \n", estado_barber);
      printf("cantidad clientes barbero.0 %d \n", cantidad_clientes);
      printf("\n");
      fflush ( stdout ) ;
      usleep (100000) ;
      sem_wait(&semcliente);

	}else{
      
      cantidad_clientes--;
  		estado_barber=1;
  		printf("XXXXXXXXXXXXXXXXXX...trabajando..... \n");
      printf("estado del barbero barbero.1  %d \n", estado_barber);
      printf("cantidad clientes barbero.1  %d \n", cantidad_clientes);
      printf("\n");
      fflush ( stdout ) ;
      usleep (200000) ;
  	  sem_post(&sembarbero);
    
  		}
    }
  }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

static void * thread_cliente_function(void* arg){
  while(1){
     sem_wait(&clientes_mutex);
	  if(cantidad_clientes < sillas){
		   cantidad_clientes++;

	    if(estado_barber==1){
	       printf("leyendo revista.....esperando..... \n");
         printf("estado del barbero cliente.0  %d \n", estado_barber);
         printf("cantidad clientes cliente.0  %d \n", cantidad_clientes);
         printf("\n");
         fflush ( stdout ) ;
         usleep (100000) ;
         sem_post(&clientes_mutex);
         sem_wait(&sembarbero);
         return 0;
     
      }else if(estado_barber==0){
         printf("ejem,ejem...,cof, cof..hola hay alguien.... \n");
         printf("estado del barbero cliente.1  %d \n", estado_barber);
         printf("cantidad clientes cliente.1  %d \n", cantidad_clientes);
         printf("\n");
         fflush ( stdout ) ;
         usleep (100000) ;
         sem_post(&semcliente);
         sem_post(&clientes_mutex); 
       return 0;
      }
      }else{
	    	 printf("chau $, chau$...chauuuuuuuuuuuuuu ....\n");
         printf("estado del barbero cleinte.2  %d \n", estado_barber);
         printf("cantidad clientes cliente.2  %d \n", cantidad_clientes);
         printf("\n");
         fflush ( stdout ) ;
         usleep (100000) ;
       return 0;
	   	}
 
  }
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main(void){
    sem_init(&sembarbero, 0, 0);
    sem_init(&semcliente, 0, 0);
    sem_init(&clientes_mutex, 0, 1);
	  pthread_t thread_barber,thread_cliente;
    pthread_create(&thread_barber, NULL, *thread_barber_function, NULL);

	  for(int i=0;i<8;i++){
        pthread_create(&thread_cliente, NULL, *thread_cliente_function, NULL);
     }
     
	  pthread_join(thread_barber, NULL);
	  pthread_join(thread_cliente, NULL);

	  sem_destroy(&sembarbero);
	  sem_destroy(&semcliente);

	  printf("the fin \n");
return 0;
}