#include<stdio.h>
#include<pthread.h>
#include<semaphore.h>
#include<stdbool.h>

#define _BSD_SOURCE
#define ROJO       "\x1B[31m"
#define VERDE      "\x1B[32m"
#define AMARILLO   "\x1B[33m"
#define AZUL       "\x1B[34m"
#define MAGENTA    "\x1B[35m"
#define CYAN       "\x1B[36m"
#define RESET      "\x1B[0m"
#define NORMAL     "\x1B[0m"

int cantidad_clientes=0; // esta es la variable critica
int estado_barber=0; // 0 durmiendo 1 cortando el pelo 
int n=10; //sillas en sala de espera 
int value;

sem_t barbero;
sem_t cliente; 
sem_t clientes_mutex;

/////////////////////////////////////////////////////////////////////

static void * thread_barber_function(void* arg){

    while(true){ 
    	if(cantidad_clientes==0){
   	    estado_barber=0;
	    printf(VERDE"Barbero durmiendo \n"RESET);
            fflush(stdout);
            
          //esperar al cliente, wait(semcliente), semcliente=0
            sem_wait(&cliente);
            
	}else{
  		cantidad_clientes--;
  		estado_barber=1;
  		printf("Barbero cortando el pelo a cliente\n "RESET);	
                //empezar a cortar al cliente, sempost(sembarbero)
  	        fflush(stdout);
                 
                //estado_barber=0; 
                sem_post(&barbero);	
               
             }
       
      }
}
//////////////////////////////////////////////////////////////////////

static void * thread_cliente_function(void* arg){
        while(true){
      	    if(cantidad_clientes < n){
	        cantidad_clientes++;
	        if(estado_barber==1){
	            printf(AMARILLO"Barbero: ocupado cliente/s: leyendo revistas  Clientes esperando: %d\n"RESET"\n",cantidad_clientes);
                   //el cliente se pone a esperar, wait(sembarbero)
                     fflush(stdout);
                      
                     sem_post(&clientes_mutex); 
                     sem_wait(&barbero);
                     
                 }else if(estado_barber==0&&cantidad_clientes==1){ // esta durmiendo el barbero 
                          printf(CYAN"\tdespierta al barbero cof cof \n"RESET);
                        //restablece el valor sem_wait(&cliente) a 1 con sempost(semcliente)
                          fflush(stdout);
                           
                          sem_post(&cliente);
                          sem_post(&clientes_mutex);
                          return 0; 
                }
            }else{
	    	printf(ROJO"cliente se va sin cortar el pelo sala de espera completa! \n"RESET); 

                fflush(stdout);
               
                return 0;
  	    	 }
            
        }
      
  }
///////////////////////////////////////////////////////////////////////

int main(void){
 
      sem_init(&barbero,0,0);
      sem_init(&cliente,0,0);
      sem_init(&clientes_mutex,0,1); 
      sem_getvalue(&barbero, &value);
      sem_getvalue(&cliente, &value); 

 
	pthread_t thread_barber,thread_cliente;

	for(int i=0;i<20;i++){
    pthread_create(&thread_barber, NULL, *thread_barber_function, NULL);
    pthread_create(&thread_cliente, NULL, *thread_cliente_function, NULL);
    
        }
	pthread_join(thread_barber, NULL);
	pthread_join(thread_cliente, NULL);

	sem_destroy(&barbero);
        sem_destroy(&cliente);

	printf(CYAN" fin \n"RESET);
//        printf(NORMAL""RESET);
        return 0;
        
}
